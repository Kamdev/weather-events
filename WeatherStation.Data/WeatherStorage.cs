﻿using System;

namespace Temperature
{
    public class WeatherStorage
    {
        public WeatherStorage(Temperature temperature, Humidity humidity, Pressure pressure)
        {
            Temperature = temperature;
            Humidity = humidity;
            Pressure = pressure;
        }
        
        public Temperature Temperature { get; private set; }
        
        public Humidity Humidity { get; private set; }

        public Pressure Pressure { get; private set; }

        public void UpdateTemperature(Temperature temperature)
            => Temperature = temperature ?? throw new ArgumentNullException(nameof(temperature));

        public void UpdateHumidity(Humidity humidity)
            => Humidity = humidity ?? throw new ArgumentNullException(nameof(humidity));

        public void UpdatePressure(Pressure pressure)
            => Pressure = pressure ?? throw new ArgumentNullException(nameof(pressure));
    }
}