﻿namespace Temperature
{
    public class AppSettings
    {
        private static AppSettings appSettings;
        public const int MaximalHumidityValue = 100;
        public const int MinimalHumidityValue = 0;

        public static AppSettings GetSettings() => appSettings ?? new AppSettings();
        
        private AppSettings()
        {
            MaximalTemperatureInCelsius = 100;
            MinimalTemperatureInCelsius = -100;
            TemperatureAccuracy = 0.1;
            MinimalPressure = 650;
            MaximalPressure = 900;
        }
        
        public double MaximalTemperatureInCelsius { get; set; }
        
        public double MinimalTemperatureInCelsius { get; set; }
        
        public int MinimalPressure { get; set; }

        public int MaximalPressure { get; set; }
        
        public double TemperatureAccuracy { get; set; }
    }
}