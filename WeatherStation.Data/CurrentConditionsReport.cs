﻿using Temperature.EventArgs;

namespace Temperature
{
    public class CurrentConditionsReport
    {
        public Temperature CurrentTemperature { get; private set; }
        
        public Humidity CurrentHumidity { get; private set; }
        
        public Pressure CurrentPressure { get; private set; }

        public CurrentConditionsReport(Temperature currentTemperature, Humidity currentHumidity, Pressure currentPressure)
        {
            CurrentTemperature = currentTemperature;
            CurrentHumidity = currentHumidity;
            CurrentPressure = currentPressure;
        }

        public void UpdateTemperature(object sender, TemperatureEventArgs args)
        {
            CurrentTemperature = args.CurrentTemperature;
        }
        
        public void UpdateHumidity(object sender, HumidityEventArgs args)
        {
            CurrentHumidity = args.CurrentHumidity;
        }

        public void UpdatePressure(object sender, PressureEventArgs args)
        {
            CurrentPressure = args.CurrentPressure;
        }
    }
}