﻿using System;

namespace Temperature
{
    public class Humidity : ICloneable
    {
        private int humidityInPercents;

        public Humidity(int humidityInPercents)
        {
            HumidityInPercents = humidityInPercents;
            AppSettings = AppSettings.GetSettings();
        }
        
        private AppSettings AppSettings { get; }

        public int HumidityInPercents
        {
            get => humidityInPercents; 
            set => humidityInPercents = value > AppSettings.MaximalHumidityValue 
                   || value < AppSettings.MinimalHumidityValue 
                ? throw new ArgumentOutOfRangeException(nameof(value), 
                    $"Humidity must be in range from {AppSettings.MinimalHumidityValue} to {AppSettings.MinimalHumidityValue}.") 
                : value;
        }

        public bool Equals(Humidity humidity)
            => humidity is { } && humidity.HumidityInPercents == HumidityInPercents;

        public override bool Equals(object humidityObject)
            => Equals(humidityObject as Humidity);

        public override int GetHashCode()
        {
            int hash = 23;
            
            return hash * HumidityInPercents.GetHashCode();
        }

        public override string ToString()
            => $"{HumidityInPercents}%";

        public object Clone() => new Humidity(HumidityInPercents);
        
        public static bool operator ==(Humidity lhs, Humidity rhs)
            => lhs is null && rhs is null || lhs is { } && lhs.Equals(rhs);

        public static bool operator !=(Humidity lhs, Humidity rhs) => !(lhs == rhs);

        public static int operator -(Humidity lhs, Humidity rhs)
            => Subtract(lhs, rhs);
        
        public static int operator +(Humidity lhs, Humidity rhs)
            => Add(lhs, rhs);

        public static int Subtract(Humidity lhs, Humidity rhs)
        {
            if (lhs is null)
            {
                throw new ArgumentNullException(nameof(lhs));
            }

            if (rhs is null)
            {
                throw new ArgumentNullException(nameof(rhs));
            }

            return lhs.HumidityInPercents - rhs.HumidityInPercents;
        }
        
        public static int Add(Humidity lhs, Humidity rhs)
        {
            if (lhs is null)
            {
                throw new ArgumentNullException(nameof(lhs));
            }

            if (rhs is null)
            {
                throw new ArgumentNullException(nameof(rhs));
            }

            return lhs.HumidityInPercents - rhs.HumidityInPercents;
        }
    }
}