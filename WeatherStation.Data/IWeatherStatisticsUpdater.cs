﻿using System;
using Temperature.EventArgs;

namespace Temperature
{
    public interface IWeatherStatisticsUpdater
    {
        event EventHandler<TemperatureEventArgs> TemperatureChanged;
        event EventHandler<HumidityEventArgs> HumidityChanged;
        event EventHandler<PressureEventArgs> PressureChanged;

        void UpdateTemperature(Temperature temperature);
        void UpdateHumidity(Humidity humidity);
        void UpdatePressure(Pressure pressure);
    }
}