﻿using System;

namespace Temperature
{
    public class Pressure : ICloneable
    {
        private int pressureValueInMmHg;

        public Pressure(int pressureValueInMmHg)
        {
            PressureValueInMmHg = pressureValueInMmHg;
            AppSettings = AppSettings.GetSettings();
        }

        public int PressureValueInMmHg 
        { 
            get => pressureValueInMmHg;
            set => pressureValueInMmHg = value > AppSettings.MaximalPressure 
                                         || value < AppSettings.MinimalPressure 
                ? throw new ArgumentOutOfRangeException(nameof(value), 
                    $"Pressure must be in range from {AppSettings.MinimalPressure} to {AppSettings.MaximalPressure} mmHg.") 
                : value;
        }
        
        private AppSettings AppSettings { get; }


        public bool Equals(Pressure pressure)
            => pressure is { } && pressure.PressureValueInMmHg == PressureValueInMmHg;

        public override bool Equals(object pressureObject)
            => Equals(pressureObject as Pressure);

        public override int GetHashCode()
        {
            int hash = 11;
            
            return hash * PressureValueInMmHg.GetHashCode();
        }

        public override string ToString()
            => $"{PressureValueInMmHg}mmHg";

        public object Clone() => new Pressure(PressureValueInMmHg);
        
        public static bool operator ==(Pressure lhs, Pressure rhs)
            => lhs is null && rhs is null || lhs is { } && lhs.Equals(rhs);

        public static bool operator !=(Pressure lhs, Pressure rhs) => !(lhs == rhs);

        public static int operator -(Pressure lhs, Pressure rhs)
            => Subtract(lhs, rhs);
        
        public static int operator +(Pressure lhs, Pressure rhs)
            => Add(lhs, rhs);

        public static int Subtract(Pressure lhs, Pressure rhs)
        {
            if (lhs is null)
            {
                throw new ArgumentNullException(nameof(lhs));
            }

            if (rhs is null)
            {
                throw new ArgumentNullException(nameof(rhs));
            }

            return lhs.PressureValueInMmHg - rhs.PressureValueInMmHg;
        }
        
        public static int Add(Pressure lhs, Pressure rhs)
        {
            if (lhs is null)
            {
                throw new ArgumentNullException(nameof(lhs));
            }

            if (rhs is null)
            {
                throw new ArgumentNullException(nameof(rhs));
            }

            return lhs.PressureValueInMmHg - rhs.PressureValueInMmHg;
        }
    }
}