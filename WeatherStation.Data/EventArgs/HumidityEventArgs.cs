﻿namespace Temperature.EventArgs
{
    public class HumidityEventArgs
    {
        public HumidityEventArgs(Humidity currentHumidity, Humidity previousHumidity)
        {
            CurrentHumidity = currentHumidity;
            PreviousHumidity = previousHumidity;
        }
        
        public Humidity CurrentHumidity { get; }
        
        public Humidity PreviousHumidity { get; }
    }
}