﻿namespace Temperature.EventArgs
{
    public class PressureEventArgs
    {
        public PressureEventArgs(Pressure currentPressure, Pressure previousPressure)
        {
            CurrentPressure = currentPressure;
            PreviousPressure = previousPressure;
        }
        
        public Pressure CurrentPressure { get; }

        public Pressure PreviousPressure { get; }
    }
}