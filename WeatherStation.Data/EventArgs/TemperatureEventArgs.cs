﻿using System;

namespace Temperature.EventArgs
{
    public class TemperatureEventArgs : System.EventArgs
    {
        public TemperatureEventArgs(Temperature updated, Temperature previous)
        {
            CurrentTemperature = updated;
            PreviousTemperature = previous;
        }
        
        public Temperature CurrentTemperature { get; }
        
        public Temperature PreviousTemperature { get; }
    }
}