﻿using System.IO;
using System.Text.Json;
using Temperature.EventArgs;

namespace Temperature
{
    public class StatisticReport
    {
        private string weatherDataFileName;

        public StatisticReport(Temperature temperature, Humidity humidity, Pressure pressure, string weatherDataFileName)
        {
            CurrentWeatherData = new WeatherStorage(temperature, humidity, pressure);
            this.weatherDataFileName = weatherDataFileName;
        }

        private WeatherStorage CurrentWeatherData { get; }

        public void UpdateTemperature(object sender, TemperatureEventArgs args)
        {
            CurrentWeatherData.UpdateTemperature(args.CurrentTemperature);
        }
    }
}