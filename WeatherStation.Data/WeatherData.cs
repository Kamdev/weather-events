﻿using System;
using Temperature.EventArgs;

namespace Temperature
{
    public class WeatherData : IWeatherStatisticsUpdater
    {
        public WeatherData(Temperature temperature, Humidity humidity, Pressure pressure)
        {
            Temperature = temperature;
            Humidity = humidity;
            Pressure = pressure;
        }
        
        public Temperature Temperature { get; private set; }
        
        public Humidity Humidity { get; private set; }
        
        public Pressure Pressure { get; set; }
        
        public event EventHandler<TemperatureEventArgs> TemperatureChanged;
        public event EventHandler<HumidityEventArgs> HumidityChanged;
        public event EventHandler<PressureEventArgs> PressureChanged;
        
        public void UpdateTemperature(Temperature temperature)
        {
            if (temperature != Temperature)
            {
                TemperatureEventArgs args = new TemperatureEventArgs((Temperature) temperature.Clone(), Temperature);
                Temperature = temperature;
                TemperatureChanged?.Invoke(this, args);
            }
        }

        public void UpdateHumidity(Humidity humidity)
        {
            if (humidity != Humidity)
            {
                HumidityEventArgs args = new HumidityEventArgs((Humidity) humidity.Clone(), Humidity);
                Humidity = humidity;
                HumidityChanged?.Invoke(this, args);
            }
        }

        public void UpdatePressure(Pressure pressure)
        {
            if (pressure != Pressure)
            {
                PressureEventArgs args = new PressureEventArgs((Pressure) pressure.Clone(), Pressure);
                Pressure = pressure;
                PressureChanged?.Invoke(this, args);
            }
        }
    }
}